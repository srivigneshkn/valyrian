#===============================================================================
#Title           :Aws_DataPipeline_Readme.md
#Author          :Srivignesh KN
#Date            :20161201
#Version         :0.1
#================================================================================

DataPipelineReadMe :
-------------------

	1. This data pipeline is used to load data from S3 into Redshift on a event based schedule.
	
Components involved :
----------------------
	1. AWS S3
	2. AWS Data Pipeline
	3. AWS Redshift
	4. AWS Lamdba
	5. AWS SNS Service 
	
UseCase : 
----------
	1. When new files are published in S3 buckets , the lambda functions are invoked to trigger the datapipeline.
	2. The data pipeline takes data from S3 and loads into Redshift.
	3. Data Pipeline uses redshift copy command in order to move data.
	4. Lamdba function is coded using python.
	5. SNS service is configured to trigger either notify via email or through SMS.
	
Note :
----------
	1. The code example has Data Pipeline configs and Lambda configs.
	2. SNS Service and S3 buckets needs to be configured accordingly as per the requirements.