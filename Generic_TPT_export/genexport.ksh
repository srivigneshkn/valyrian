SOURCE_DB=$1
SOURCE_TABLE=$2
timestamp=$( date +%T )
CURRENT_PATH=`pwd`
CURRENT_DT=`date '+%Y%m%d%H%M%S'`
LOG_PATH=`pwd`/logs
LOG_FILE="$SOURCE_DB""_""$SOURCE_TABLE""_""$CURRENT_DT"".log"
LOGON_FILE=Gen_Logon.out
OUTPUT_PATH=`pwd`/out
CONTROL_PATH=`pwd`/ctl


source config.sh

#TPT_PATH=/opt/teradata/client/14.10/tbuild/bin/
#TPT_CHECKPOINT=/opt/teradata/client/14.10/tbuild/checkpoint
#TPT_LOGS=/opt/teradata/client/14.10/tbuild/logs
#TD_WHERECLAUSE=`echo 1=1`

echo "Source DB is" $SOURCE_DB
echo "Source Table is " $SOURCE_TABLE
echo "current path is " $CURRENT_PATH


rm ${CURRENT_PATH}/${SOURCE_TABLE}_export1.out


bteq <<EOF >>${CURRENT_PATH}/logs/${LOG_FILE} 2>&1
.SET RETRY OFF
.SET SESSION TRANSACTION BTET
.RUN FILE ${CURRENT_PATH}/${LOGON_FILE}
.SET TITLEDASHES OFF
.SET WIdth 254
.os echo "Inside Bteq"
select current_timestamp(0);
-- Exit if logon failed

.if errorcode > 0 then .quit 100

.export file=${CURRENT_PATH}/${SOURCE_TABLE}_export1.out
.set format off
.set echoreq off
.set recordmode off
select columnname,
case when columntype='CF' /*Char*/ then 'Varchar('||trim(both from ColumnLength+2)||')'
when columntype='CV' /*Varchar*/ then 'Varchar('||trim(both from ColumnLength)||')'
when columntype='I' /*Integer*/ then 'Varchar(10)'
when columntype='I1' /*Byte Int*/ then 'Varchar(10)'
when columntype='I2' /*Small Int*/ then 'Varchar(10)'
when columntype='I8' /*BigInt*/ then 'Varchar(10)'
when columntype='D' /*Decimal*/ then 'Varchar(21)'
when columntype='DA' /*Date*/ then 'Varchar(10)'
when columntype='AT' /*Time*/ then 'Varchar(8)'
when columntype='TS' /*Timestamp*/ then 'Varchar(30)'
when columntype='SZ' /*Timestamp with Timezone*/ then 'Varchar(35)'
when columntype='PD' /*Period Date*/ then 'Varchar(10)'
when columntype='PM' /*Period Timestamp with Timezone*/ then 'Varchar(50)'
when columntype='PS' /*Period Timestamp*/ then 'Varchar(50)'
when columntype='PT' /*Period Time*/ then 'Varchar(30)'
when columntype='PZ' /*Period Time with Timezone*/ then 'Varchar(30)'
 else 'Varchar('||trim(both from ColumnLength)||')' /*More datatypes to be added in the Future*/ end as columntype,
case when columnid=(select max(columnid) from dbc.columns where databasename='$SOURCE_DB' and tablename='$SOURCE_TABLE') then ' ' else ',' end as Delimiter
from dbc.columns where databasename='$SOURCE_DB' and tablename='$SOURCE_TABLE' order by columnid;

.export reset
.set echoreq on
.quit

EOF

TD_SCHEMADEFINITION=`echo "$(tail -n +3 ${CURRENT_PATH}/${SOURCE_TABLE}_export1.out)"`

echo "Schema Definition is " $TD_SCHEMADEFINITION

rm ${CURRENT_PATH}/${SOURCE_TABLE}_export2.out

# TPT Export Schema definition.

bteq <<EOF >>${CURRENT_PATH}/logs/${LOG_FILE} 2>&1
.SET RETRY OFF
.SET SESSION TRANSACTION BTET
.RUN FILE ${CURRENT_PATH}/${LOGON_FILE}
.SET TITLEDASHES OFF
.SET WIdth 254

select current_timestamp(0);
-- Exit if logon failed

.if errorcode > 0 then .quit 100

.export file=${CURRENT_PATH}/${SOURCE_TABLE}_export2.out
.set format off
.set echoreq off
.set recordmode off

select trim(both from 'cast (') || trim(both from columnname)|| ' as ' ||case when columntype='CF' /*Char*/ then 'Varchar('||trim(both from ColumnLength+2)||')'
when columntype='CV' /*Varchar*/ then 'Varchar('||trim(both from ColumnLength)||')'
when columntype='I' /*Integer*/ then 'Varchar(10)'
when columntype='I1' /*Byte Int*/ then 'Varchar(10)'
when columntype='I2' /*Small Int*/ then 'Varchar(10)'
when columntype='I8' /*BigInt*/ then 'Varchar(10)'
when columntype='D' /*Decimal*/ then 'Varchar(21)'
when columntype='DA' /*Date*/ then 'Varchar(10)'
when columntype='AT' /*Time*/ then 'Varchar(8)'
when columntype='TS' /*Timestamp*/ then 'Varchar(30)'
when columntype='SZ' /*Timestamp with Timezone*/ then 'Varchar(35)'
when columntype='PD' /*Period Date*/ then 'Varchar(10)'
when columntype='PM' /*Period Timestamp with Timezone*/ then 'Varchar(50)'
when columntype='PS' /*Period Timestamp*/ then 'Varchar(50)'
when columntype='PT' /*Period Time*/ then 'Varchar(30)'
when columntype='PZ' /*Period Time with Timezone*/ then 'Varchar(30)'
 else 'Varchar('||trim(both from ColumnLength)||')' /*More datatypes to be added in the Future*/  end ||
 ')' ||
case when columnid=(select max(columnid) from dbc.columns where databasename='$SOURCE_DB' and tablename='$SOURCE_TABLE') then ' ' else ',' end as FinalExp
from dbc.columns where databasename='$SOURCE_DB' and tablename='$SOURCE_TABLE' order by columnid;

.export reset
.set echoreq on
.quit

EOF

TD_SCHEMAEXPORTDEFINITION=`echo "$(tail -n +3 ${CURRENT_PATH}/${SOURCE_TABLE}_export2.out)"`

TD_FILENAME=`echo $SOURCE_TABLE`

sed -e "s/TDARG_TABLENAME/`echo $SOURCE_TABLE`/g" -e "s/TDARG_SCHEMADEFINITION/`echo $TD_SCHEMADEFINITION`/g" -e "s/TDARG_SCHEMAEXPORTDEFINITION/`echo $TD_SCHEMAEXPORTDEFINITION`/g" -e "s/SOURCE_DB/`echo $SOURCE_DB`/g" -e "s/SOURCE_TABLE/`echo $SOURCE_TABLE`/g" -e "s/TDARG_FILENAME/`echo $TD_FILENAME`/g" -e "s/TDARG_WHERE_CLAUSE/`echo $TD_WHERECLAUSE`/g" < Header1.out > $SOURCE_TABLE.ctl

echo "Initializing TPT"
echo "Clearing Restartability files"
rm $TPT_CHECKPOINT/*
echo "Clearing Checkpoint files"
rm $TPT_LOGS/*
echo "Invoking TPT"

$TPT_PATH/tbuild -f $SOURCE_TABLE.ctl -v TPTParameter.param -s 1

mv ${CURRENT_PATH}/${SOURCE_TABLE}.ctl ${CONTROL_PATH}/${SOURCE_TABLE}.ctl
mv ${CURRENT_PATH}/${TD_FILENAME}.out ${OUTPUT_PATH}/${TD_FILENAME}.out

gzip ${OUTPUT_PATH}/${TD_FILENAME}.out


rm ${CURRENT_PATH}/${SOURCE_TABLE}_export1.out
rm ${CURRENT_PATH}/${SOURCE_TABLE}_export2.out
