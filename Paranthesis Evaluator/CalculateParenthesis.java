package calculateparenthesis;

public class CalculateParenthesis {

	public static void main(String[] args) {
		/*  
		 * Program Usage :
		 * The Program is used to validate the open and close parenthesis in a given expression.
		 * Logic :
		 *           There are 3 different gates used in the logic.
		 *           Gate 1 :
		 *           			If there are odd number of characters in the input string , the expression is termed as false.
		 *           Gate 2 :
		 *           			If the first character is a close bracket, the expression is termed as false.
		 *           Gate 3 :
		 *           			The count of open and close brackets are calculated.
		 *            			When the open bracket count is equal to close bracket count then the expression is termed as True else it is termed as False.
		 * Variables:
		 * 
		 * Input variable is stored in the string variable "s".
		 * Two variables - currchar, nextchar are used to store the current and the next characters of a string.
		 * Two Counter variables, opencount and closecount are used to count the open and close braces.
		 * Variables- CharLength and firstchar are used to store the character length and the first character respectively.
		 * Two Counter variables - opencount and closecount are used to count the open and close braces.
		 * 
		 * Input - Output Validation :
		 *  ()()()     -- True
		 *  ((()))     -- True
 		 *	()(())     -- True
 		 *  ))()))     -- False
		 *  ()()(()(   -- False    
		*/

		String s="()()(())";
		char firstchar=s.charAt(0);
		char currchar,nextchar;
		int opencount = 0, closecount=0;
		
		
		int CharLength=s.length();
		
		if (CharLength%2!=0) 
		{
		   System.out.println("Expression" +s+ "is false");
		}
		else if (firstchar==')')
		{
			System.out.println("Expression" +s+ "is false");
		}
		else
		{
			for(int i=0;i<CharLength;i++)
			{
		      currchar=s.charAt(i);
		      if(currchar=='(')
		      {
		    	  opencount=opencount+1;
		      }
		      else if (currchar==')')
		      {
		    	  closecount=closecount+1;
		      }
			}
			
			if (opencount == closecount)
			System.out.println("Expression" +s+ "is true");
			else
				System.out.println("Expression" +s+ "is false");
		}
	}
}
	
