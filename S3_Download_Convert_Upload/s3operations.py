#!/usr/bin/env python
#Title           :s3operations.py
#Description     :This script performs the following operations.
#                    1. Fetch data from S3 to Local Bucket
#                    2. Convert the .csv files to .xls files.
#                    3. Upload converted files into S3.
#                    4. Delete the files from the local folder.
#Author          :Srivignesh KN
#Date            :20161121
#Version         :0.1
#Usage           :python s3operations.py
#Notes           :AWS Keys are maintained in the script, to be chenged to environment files.
#Python_Version  :2.7.10
#==============================================================================

import boto
import sys, os
from boto.s3.key import Key
from boto.exception import S3ResponseError
import time, datetime, os, shutil
import csv
import openpyxl

# AWS Keys
AWS_ACCESS_KEY_ID= os.getenv("KEYID") 
AWS_ACCESS_SECRET_KEY = os.getenv("KEYPASS")

BUCKET_NAME = "MyBucketName"         # Bucket Name to connect to.
IES_KEY_NAME = "MyFolderName/"                  # Folder Name from which data needs to be downloaded.
IES_TARGET_KEY_NAME = "MyTargetFolderName/"   # Folder Name into which the converted data needs to be uploaded.
DOWNLOAD_LOCATION_PATH = "/Users/srivignesh/Documents/Docs/IAI/Python/IES/XLTest/"           #Base Download Location Path
DESTINATION_CSV_PATH = "/Users/srivignesh/Documents/Docs/IAI/Python/IES/XLTest/Talend/"      #Base Path with the Key Appended.
TARGET_XLS_PATH = "/Users/srivignesh/Documents/Docs/IAI/Python/Convertedfiles/"              # Target Path where the xlsx files needs to be stored.

# Create the Source and Target Directories if it doesnot exists.

if not os.path.exists(DOWNLOAD_LOCATION_PATH):
    print ("Making download directory")
    os.mkdir(DOWNLOAD_LOCATION_PATH)

if not os.path.exists(TARGET_XLS_PATH):
    print ("Making Conversion directory")
    os.mkdir(TARGET_XLS_PATH)

# Module to Download the files from S3 Bucket

def download_s3_files():
    conn  = boto.connect_s3(AWS_ACCESS_KEY_ID, AWS_ACCESS_SECRET_KEY)
    bucket = conn.get_bucket(BUCKET_NAME)

    #Looping through the list of files.
    bucket_list = bucket.list(prefix=IES_KEY_NAME, delimiter='/')
    for l in bucket_list:
        key_string = str(l.key)
        s3_path = DOWNLOAD_LOCATION_PATH + key_string
        # Error out if the file is not downloaded locally.
        try:
            print ("Current File is ", s3_path)
            l.get_contents_to_filename(s3_path)
        except (OSError,S3ResponseError) as e:
            pass
            # check if the file has been downloaded locally
            if not os.path.exists(s3_path):
                try:
                    os.makedirs(s3_path)
                except OSError as exc:
                    import errno
                    if exc.errno != errno.EEXIST:
                        raise

# Module to Convert the Csv Files to XLS.

def convert_csv_xls():

# Loops through the directory for .Csv files, if it is a csv file, convert to xls else pass to next file.

 for filename in os.listdir(DESTINATION_CSV_PATH):
   if filename.endswith(".csv"):
       print "Csv File, "+filename
       workbook = openpyxl.Workbook()
       worksheet = workbook.active
       conversionfile = open(DESTINATION_CSV_PATH+"/"+filename)
       reader = csv.reader(conversionfile, delimiter=',')
       basename, ext = os.path.splitext(filename)
       print basename+".xlsx"
       targetfilename = basename+".xlsx"
       for row in reader:
         worksheet.append(row)
       conversionfile.close()
       workbook.save(TARGET_XLS_PATH+"/"+targetfilename)
   else:
      print filename+"Not a CSV File, moving to next file"

# Module to Upload files to S3.
 
def upload_s3_files():

# Loops through the list of files and upload the .xlsx files alone into S3.

    print "Beginning Upload"
    conn  = boto.connect_s3(AWS_ACCESS_KEY_ID, AWS_ACCESS_SECRET_KEY)
    bucket = conn.get_bucket(BUCKET_NAME)
    for filename in os.listdir(TARGET_XLS_PATH):
       if filename.endswith(".xlsx"):
          print "Current xlsx file"+filename
          full_key_name = os.path.join(IES_TARGET_KEY_NAME,filename)
          uploadkey = bucket.new_key(full_key_name)
          uploadkey.set_contents_from_filename(TARGET_XLS_PATH+"/"+filename)

# Module to Delete files from the local

def delete_s3_files():

# Loop through the downloaded directory and clear the files for next run.

    print "Preparing to delete from."+TARGET_XLS_PATH
    for filename in os.listdir(TARGET_XLS_PATH):
        abs_file_path = os.path.join(TARGET_XLS_PATH,filename)
        try:
             if os.path.isfile(abs_file_path):
                os.unlink(abs_file_path)
        except Exception as e:
             print(e)



if __name__ == '__main__':
     download_s3_files()
     convert_csv_xls()
     upload_s3_files()
     delete_s3_files()